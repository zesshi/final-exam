package com.example.myapplication
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class freinds : AppCompatActivity() {
    private lateinit var userRecyclerView: RecyclerView
    private lateinit var userList: ArrayList<UserInfo>
    private lateinit var adapter:UserAdapter
    private lateinit var auth: FirebaseAuth
    private lateinit var myDataRef: DatabaseReference
    private lateinit var goToProfile: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)
        userList = ArrayList()
        myDataRef = FirebaseDatabase.getInstance().getReference()
        adapter= UserAdapter(this,userList)
        auth = FirebaseAuth.getInstance()
        userRecyclerView = findViewById(R.id.userRecylerView)
        userRecyclerView.layoutManager = LinearLayoutManager(this)
        userRecyclerView.adapter = adapter
        goToProfile = findViewById(R.id.goToProfile)

        goToProfile.setOnClickListener {
            val pendingIntent = NavDeepLinkBuilder(this.applicationContext)
                .setGraph(R.navigation.my_nav)
                .setDestination(R.id.profile_fragment)
                .createPendingIntent()

            pendingIntent.send()
        }

        myDataRef.child("UserInfo").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                userList.clear()
                for (postSnapshot in snapshot.children){
                    val currentUser = postSnapshot.getValue(UserInfo::class.java)

                    if(auth.currentUser?.uid != currentUser?.uid){
                        userList.add(currentUser!!)
                    }

                }
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
}