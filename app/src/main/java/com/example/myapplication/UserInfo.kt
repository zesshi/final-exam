package com.example.myapplication


data class UserInfo(
    var PersonName: String = "",
    var imageUrl: String = "",
    var email: String = "",
    var uid: String = ""



){
    constructor(PersonName: String, email: String,uid: String) : this() {
        this.PersonName= PersonName
        this.email = email
        this.uid = uid
    }
}
