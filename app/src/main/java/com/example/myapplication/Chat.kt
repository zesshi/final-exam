package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class Chat : AppCompatActivity() {
    private lateinit var chatRecyclerView: RecyclerView
    private lateinit var messageBox: EditText
    private lateinit var sendBttn : ImageView
    private lateinit var messageAdapter: messageAdapter
    private lateinit var messageList: ArrayList<message>
    private lateinit var myDataRef: DatabaseReference

    var reciverBox: String? = null
    var senderBox: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)


        val name = intent.getStringExtra("name")
        val reciverUid = intent.getStringExtra("uid")
        val senderUid = FirebaseAuth.getInstance().currentUser?.uid
        myDataRef = FirebaseDatabase.getInstance().getReference()
        senderBox = reciverUid + senderUid
        reciverBox = senderUid + reciverUid
        supportActionBar?.title = name

        chatRecyclerView = findViewById(R.id.chatRecyclerView)
        messageBox = findViewById(R.id.messageBox)
        sendBttn = findViewById(R.id.sendBttn)
        messageList = ArrayList()
        messageAdapter = messageAdapter(this,messageList)
        chatRecyclerView.layoutManager = LinearLayoutManager(this)
        chatRecyclerView.adapter = messageAdapter

        myDataRef.child("chats").child(senderBox!!).child("messages")
            .addValueEventListener(object: ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    messageList.clear()
                    for (postSnapshot in snapshot.children){
                        val message = postSnapshot.getValue(message::class.java)
                        messageList.add(message!!)
                    }
                    messageAdapter.notifyDataSetChanged()
                }

                override fun onCancelled(error: DatabaseError) {

                }

            })

        sendBttn.setOnClickListener {
            val message = messageBox.text.toString()
            val messageObj = message(message,senderUid)
            myDataRef.child("chats").child(senderBox!!).child("messages").push()
                .setValue(messageObj).addOnSuccessListener {
                    myDataRef.child("chats").child(reciverBox!!).child("messages").push()
                        .setValue(messageObj)
                }
            messageBox.setText("")

        }
    }
}