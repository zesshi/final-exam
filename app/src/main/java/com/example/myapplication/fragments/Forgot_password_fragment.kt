package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth

class Forgot_password_fragment:Fragment(R.layout.fragment_forgot_password) {
    private lateinit var email: EditText
    private lateinit var send: Button
    private lateinit var mAuth: FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email = view.findViewById(R.id.editEmail)
        send = view.findViewById(R.id.send)
        mAuth = FirebaseAuth.getInstance()

        send.setOnClickListener {
            val mail = email.text.toString().trim()

            if(android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
                mAuth.sendPasswordResetEmail(mail).addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        Toast.makeText(activity, "პაროლის შეცვლის მოთხოვნა წარმატებით გაიგზავნა მეილზე", Toast.LENGTH_SHORT).show()
                        findNavController().popBackStack()

                    }
                }
            }
            else{
                Toast.makeText(activity, "მეილი შეიყვანეთ სწორი ფორმატით მაგალითად:'btu@gmail.com' ", Toast.LENGTH_SHORT).show()

            }
        }

    }
}