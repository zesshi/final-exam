package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.example.myapplication.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Register_fragment:Fragment(R.layout.fragment_register) {
    private lateinit var PersonName:EditText
    private lateinit var EmailAddress:EditText
    private lateinit var newPassword:EditText
    private lateinit var rePassword:EditText
    private lateinit var create:Button
    private lateinit var imageUrl:EditText
    private lateinit var checkbox:CheckBox
    private val mAuth = FirebaseAuth.getInstance()
    private val dbUserInfo: DatabaseReference = FirebaseDatabase.getInstance().getReference("UserInfo")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        PersonName = view.findViewById(R.id.PersonName)
        EmailAddress = view.findViewById(R.id.EmailAddress)
        newPassword = view.findViewById(R.id.newPassword)
        rePassword = view.findViewById(R.id.repeatPassword)
        create = view.findViewById(R.id.create)
        checkbox = view.findViewById(R.id.checkBox)
        imageUrl = view.findViewById(R.id.img_url)

        create.setOnClickListener {
            val email = EmailAddress.text.toString().trim()
            val password = newPassword.text.toString().trim()
            val rePassword = rePassword.text.toString().trim()
            val PersonName = PersonName.text.toString().trim()
            val imageUrl = imageUrl.text.toString().trim()
            if (PersonName.isEmpty()){
                Toast.makeText(activity, "Enter a name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else if(email.isEmpty() || !email.contains("@") || !email.contains(".")){
                Toast.makeText(activity, "Enter the correct Email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            }
            else if(password.isEmpty() || rePassword.isEmpty()){
                Toast.makeText(activity, "Enter the password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            else if(rePassword!=password){
                Toast.makeText(activity, "Please repeat the password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            else if(!checkbox.isChecked){
                Toast.makeText(activity, "Please confirm if you agree to the rules", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }else if (imageUrl.isEmpty()) {
                Toast.makeText(activity, "Please Input Image Url", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            else
                Toast.makeText(activity, "Registration completed successfully ", Toast.LENGTH_SHORT).show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,
                password
            ).
            addOnCompleteListener {task ->
                if(task.isSuccessful){

                    val userInfo = UserInfo(PersonName,imageUrl,email,mAuth.currentUser?.uid!!)
                    dbUserInfo.child(mAuth.currentUser?.uid!!).setValue(userInfo)
                }else{
                    Toast.makeText(activity, "რაღაც შეცდომაა!", Toast.LENGTH_SHORT).show()
                }
            }
            findNavController().popBackStack()
        }
        }

}