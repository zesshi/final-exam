package com.example.myapplication.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class Profile_fragment:Fragment(R.layout.fragment_profile) {
    private lateinit var name: TextView
    private lateinit var email: TextView
    private lateinit var photo : ImageView
    private lateinit var logout: Button
    private lateinit var friends: TextView
    private val dbUserInfo: DatabaseReference = FirebaseDatabase.getInstance().getReference("UserInfo")
    private val auth = FirebaseAuth.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        name = view.findViewById(R.id.name)
        email = view.findViewById(R.id.email)
        photo = view.findViewById(R.id.photo)
        logout = view.findViewById(R.id.logout)
        friends = view.findViewById(R.id.friends)

        friends.setOnClickListener {
            activity?.let{
                val intent = Intent (it, com.example.myapplication.freinds::class.java)
                it.startActivity(intent)
            }

            
        }
        logout.setOnClickListener {
            findNavController().popBackStack()
        }

        dbUserInfo.child(auth.currentUser?.uid!!).addValueEventListener(object :
            ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userinfo = snapshot.getValue(UserInfo::class.java) ?: return
                name.text = userinfo.PersonName
                email.text = userinfo.email



                Glide.with(this@Profile_fragment).load(userinfo.imageUrl).into(photo)


            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
}