package com.example.myapplication.fragments


import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.myapplication.R
import com.google.firebase.auth.FirebaseAuth


class Login_fragment:Fragment(R.layout.fragment_login) {
    private lateinit var email:EditText
    private lateinit var password:EditText
    private lateinit var forgotPassword:TextView
    private lateinit var login:Button
    private lateinit var signup:Button
    private lateinit var auth:FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.password)
        forgotPassword = view.findViewById(R.id.forgetPassword)
        login = view.findViewById(R.id.login)
        signup = view.findViewById(R.id.signUp)
        auth = FirebaseAuth.getInstance()

        forgotPassword.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.navigateToForgotPasswordFromLogin)
        }

        login.setOnClickListener {
            val email = email.text.toString()
            val pass = password.text.toString()
            if (email.isEmpty() || !email.contains("@") || !email.contains(".")) {
                Toast.makeText(
                    activity,
                    "Please input correct Email",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            } else if (pass.isEmpty()) {
                Toast.makeText(activity, "Please enter password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email,pass).addOnCompleteListener{
                    task ->
                if (task.isSuccessful){
                    findNavController().navigate(R.id.navigateToProfileFromLogin)
                }else
                    Toast.makeText(activity, "For the first time please register", Toast.LENGTH_SHORT).show()
            }


        }
        signup.setOnClickListener {
            findNavController().navigate(R.id.navigateToRegisterFromLogin)
        }








    }
    private fun validLogin(email: String, pass: String) {

    }
}
